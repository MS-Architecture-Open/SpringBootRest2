package com.adm.ats.entity;

import com.adm.ats.entity.BaseEntity;

import javax.persistence.Entity;
import java.util.Objects;

@Entity
public class Coche extends BaseEntity {

    private String modelo;

    private String marca;

    private String tipocombustion;

    private float precio;

    public Coche () {
        super();
    }

    public Coche(String modelo, String marca, String tipocombustion, float precio) {
        this();
        this.modelo = modelo;
        this.marca = marca;
        this.tipocombustion = tipocombustion;
        this.precio = precio;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getTipocombustion() {
        return tipocombustion;
    }

    public void setTipocombustion(String tipocombustion) {
        this.tipocombustion = tipocombustion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Coche)) return false;
        Coche coche = (Coche) o;
        return Float.compare(coche.getPrecio(), getPrecio()) == 0 &&
                Objects.equals(getModelo(), coche.getModelo()) &&
                Objects.equals(getMarca(), coche.getMarca()) &&
                Objects.equals(getTipocombustion(), coche.getTipocombustion());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getModelo(), getMarca(), getTipocombustion(), getPrecio());
    }

    @Override
    public String toString() {
        return "Coche{" +
                "modelo='" + modelo + '\'' +
                ", marca='" + marca + '\'' +
                ", tipocombustion='" + tipocombustion + '\'' +
                ", precio=" + precio +
                '}';
    }
}
