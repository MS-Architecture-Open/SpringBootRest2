package com.adm.ats.service;

import com.adm.ats.entity.Coche;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;


public interface CocheRepository extends CrudRepository<Coche, Long>{


}
