package com.adm.ats.service;

import com.adm.ats.entity.Coche;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;

@Component

public class DatabaseLoader implements ApplicationRunner {


    private CocheRepository cocherepo;


    @Autowired
    public DatabaseLoader(CocheRepository cocherepo) {
        this.cocherepo = cocherepo;
    }


    public void run(ApplicationArguments args) throws Exception {

        ArrayList<Coche> cargaOfCoche = new ArrayList<Coche>();

        cargaOfCoche.add(new  Coche("Rav4","Toyota","Hybrido",38000));
        cargaOfCoche.add(new Coche("Rav4","Toyota","Hybrido",38000));
        cargaOfCoche.add(new Coche("S5008","Peugeot","Gasolina",30000));
        cargaOfCoche.add(new Coche("Qasqai","Nissan","Gasolina",28000));
        cargaOfCoche.add(new Coche("Kadjar","Renault","Gasolina",25000));

        cocherepo.save(cargaOfCoche);
    }
}
